@extends('layout.master')

@section('judul')
Create Data Game
@endsection

@section('content')

<form action="/game" method="POST">
    @csrf
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Gameplay</label>
        <textarea name="gameplay" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('gameplay')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Developer</label>
        <input type="text" name="developer" class="form-control">
    </div>
    @error('developer')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Year</label>
        <input type="text" name="year" class="form-control">
    </div>
    @error('year')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection