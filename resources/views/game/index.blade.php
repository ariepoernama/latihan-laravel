@extends('layout.master')

@section('judul')
Halaman List Game
@endsection

@section('content')

<a href="/game/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Gameplay</th>
        <th scope="col">Developer</th>
        <th scope="col">Year</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($game as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->gameplay}}</td>
            <td>{{$item->developer}}</td>
            <td>{{$item->year}}</td>
            <td>
                <form action="/game/{{$item->id}}" method="POST">
                <a href="/game/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/game/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </td>
        </tr>

        @empty 
        <tr>
            <td>Data masih kosong...</td>
        </tr>

        @endforelse
    </tbody>
  </table>

  @endsection