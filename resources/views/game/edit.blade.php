@extends('layout.master')

@section('judul')
Halaman Edit game {{$game->name}}
@endsection

@section('content')

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" value="{{$game->name}}" class="form-control">
      </div>
      @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
          <label>Gameplay</label>
          <textarea name="gameplay" class="form-control" cols="30" rows="10">{{$game->gameplay}}</textarea>
      </div>
      @error('gameplay')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
          <label>Developer</label>
          <input type="text" name="developer" value="{{$game->developer}}" class="form-control">
      </div>
      @error('developer')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <div class="form-group">
          <label>Year</label>
          <input type="text" name="year" value="{{$game->year}}" class="form-control">
      </div>
      @error('year')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
  
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection