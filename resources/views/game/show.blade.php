@extends('layout.master')

@section('judul')
Halaman Detail Game {{$game->name}}
@endsection

@section('content')

<h3>{{$game->name}}</h3>
<p>{{$game->gameplay}}</p>
<p>{{$game->developer}}</p>
<p>{{$game->year}}</p>

@endsection