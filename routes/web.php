<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/form', 'FormController@bio');

Route::post('/welcome', 'FormController@kirim');

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD Game
Route::get('/game/create', 'GameController@create');
Route::post('/game', 'GameController@store');
Route::get('/game', 'GameController@index');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');
Route::delete('/game/{game_id}', 'GameController@destroy');


// CRUD Cast
Route::resource('cast', 'CastController');




